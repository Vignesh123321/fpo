$(document).ready(function() {
	$("#asstTable").dataTable({
		"processing": true,
		"paging": true
	});
});

function viewDetail(e) {

	$.ajax({
		url: '/getPageStatus?cinNo=' + e.id,
		data: JSON.stringify(""),
		dataType: "json",
		contentType: "application/json",
		type: "post",
		success: function(data) {
		 console.log(data);
			if (null != data) {
				window.location.href = "/" + data + "?id=" + e.id;
			}
			else {
				window.location.href = "/ead_main?id=" + e.id;
			}
		},
		fail: function(rs, e) {
			console.log(rs, responseText);
		}
	});
	window.location.href = "/ead_main?id=" + e.id;
}

$("table tbody tr").each(
	function() {
		var textval = $(this).find("td:eq(0)").text().trim();
		//	var textval2 = $(this).find("td:eq(1)").text().trim();
		if (textval > 1) {
			$('#' + textval).prop('disabled', true);
			//$(this).find("td:eq(0)").attr('disabled', true);
		} else {
			$("tr.rowDis input, tr.rowDis select, tr.rowDis textarea")
				.prop('disabled', true);
			//$('#submitBtn').prop('disabled', false);
		}
	});