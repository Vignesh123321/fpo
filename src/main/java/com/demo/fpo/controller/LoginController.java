package com.demo.fpo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.demo.fpo.model.Asstreport;
import com.demo.fpo.model.C_CUSITM;
import com.demo.fpo.model.DECI_REAS;
import com.demo.fpo.model.FPO_AMEND;
import com.demo.fpo.model.FPO_ITEM_DET;
import com.demo.fpo.model.FPO_MAIN;
import com.demo.fpo.model.FPO_ORDER;
import com.demo.fpo.model.FpoQueryDecision;
import com.demo.fpo.prop.PropertyFile;
import com.demo.fpo.repos.Asstreportrepost;
import com.demo.fpo.repos.C_CUSITMrepost;
import com.demo.fpo.repos.DECI_REASrepost;
import com.demo.fpo.repos.FPO_ITEM_DETrepost;
import com.demo.fpo.repos.FPO_MAINrepost;
import com.demo.fpo.repos.FPO_ORDERrepost;
import com.demo.fpo.repos.FpoQueryRepo;
import com.demo.fpo.service.FpoService;

@Controller
public class LoginController {

	@Autowired
	Asstreportrepost asstreportrepost;

	@Autowired
	C_CUSITMrepost CUSITMrepost;

	@Autowired
	FPO_MAINrepost FPOrepost;

	@Autowired
	FPO_ITEM_DETrepost FPO_ITEMrepost;

	@Autowired
	FPO_ORDERrepost FPO_ORDERrepost;

	@Autowired
	DECI_REASrepost DECI_REASrepost;

	@Autowired
	FpoService fpoService;

	public static final String offId = "12345678";

	public static final String role = "786";

	public static final String cuSite = "123123";

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView login(Model models) {
		ModelAndView modelAndView = new ModelAndView("login");
		models.addAttribute("check", "world");
		return modelAndView;
	}

	@RequestMapping("/dash")
	public String dash() {
		System.out.println("dash invoker");
		return "dash";
	}

	@RequestMapping("/pending")
	public String pending() {
		System.out.println("pending invoker");
		return "pending";
	}

	@RequestMapping("/detention")
	public String detention() {
		System.out.println("detention invoker");
		return "detention git";
	}

	@RequestMapping("/edi")
	public String edi() {
		System.out.println("edi invoker");
		return "edi";
	}

	@RequestMapping("/main")
	public ModelAndView main(Model model) {
		List<FPO_MAIN> s = FPOrepost.getview();

		ModelAndView modelAndView = new ModelAndView("main");

		model.addAttribute("check", s);

		return modelAndView;
	}

	@RequestMapping("/main_view")
	public ModelAndView main_view(HttpServletRequest request, Model model) {
		ModelAndView modelAndView = new ModelAndView("main_view");
		try {
			String id = request.getParameter("id");
			List<FPO_MAIN> s = FPOrepost.getmain(id);
			fpoService.getPojo(s.get(0));
			fpoService.setFpoMainValues(s);

			model.addAttribute("check", fpoService.getPojo(s.get(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return modelAndView;
	}

	@RequestMapping("/header")
	public String header() {
		System.out.println("header invoker");
		return "header";
	}

	@RequestMapping("/masteredit")
	public String masteredit() {
		System.out.println("masteredit invoker");
		return "masteredit";
	}

	@RequestMapping("/change")
	public String change() {
		System.out.println("change invoker");
		return "change";
	}

	@RequestMapping("/order")
	public ModelAndView order(HttpServletRequest request, Model model)

	{
		String cinNo = request.getParameter("id");
		// System.out.println("head:"+head);
		List<FPO_MAIN> s1 = FPOrepost.getmain(cinNo);
		fpoService.updateStatusOfAllPages(cinNo, s1.get(0).getITEM_ID(), "E2");
		model.addAttribute("head", s1.get(0));
		ModelAndView modelAndView = new ModelAndView("order");
		return modelAndView;
	}

	@RequestMapping("/report")
	public String report() {
		System.out.println("report invoker");
		return "report";
	}

	@RequestMapping("/comments")
	public String comments() {
		System.out.println("comments invoker");
		return "comments";
	}

	@RequestMapping("/callmemo")
	public String callmemo() {
		System.out.println("callmemo invoker");
		return "callmemo";
	}

	@Autowired
	FpoQueryRepo fpoQueryRepo;

	@RequestMapping("/editcallmemo")
	public ModelAndView someAction(@ModelAttribute("fpo_item_det") FPO_ITEM_DET fpo_item_det,
			HttpServletRequest request, Model model) {

		fpoService.insertFpoQuery(fpo_item_det);
		List<FPO_ITEM_DET> s = FPO_ITEMrepost.getitem(fpo_item_det.getCinNo());
		List<FPO_MAIN> fpoMainData = FPOrepost.getmain(fpo_item_det.getCinNo());
		List<Object> callMemo = fpoQueryRepo.getMemoData(fpo_item_det.getCinNo(), fpoQueryRepo.getMaxQueryNo());
		String getRemarks = fpoQueryRepo.getRemarks(fpo_item_det.getCinNo(), fpoQueryRepo.getMaxQueryNo());
		String DefualtQuery = fpoQueryRepo.getDefualtQuery(fpo_item_det.getCinNo(), fpoQueryRepo.getMaxQueryNo());
		List<String> defualtQueryList = fpoService.getSpecifiedDefualtQuery(DefualtQuery);
		ModelAndView modelAndView = new ModelAndView("editcallmemo");
		model.addAttribute("fpoMainData", fpoMainData.get(0));
		model.addAttribute("callMemo", callMemo);
		model.addAttribute("getRemarks", getRemarks);
		model.addAttribute("defualtQueryList", defualtQueryList);
		return modelAndView;
	}

	@Autowired
	PropertyFile propertyFile;

	@RequestMapping("/getDefualtQuery")
	public @ResponseBody List<String> getDefualtQuery() {

		return fpoService.getDefualtQuery();
	}

	@RequestMapping("/view")
	public String view() {
		System.out.println("view invoker");
		return "view";
	}

	@RequestMapping("/asse_table")
	public ModelAndView asse_table(Model model) {
		List<Asstreport> s = asstreportrepost.getNetwt();

		ModelAndView modelAndView = new ModelAndView("asse_table");

		model.addAttribute("check", s);

		return modelAndView;
	}

	@RequestMapping("/Clearance")
	public ModelAndView Clearance(Model model) {
		List<C_CUSITM> s = CUSITMrepost.getpending();

		ModelAndView modelAndView = new ModelAndView("Clearance");

		model.addAttribute("check", s);

		return modelAndView;
	}

	@RequestMapping("/asse_edit")
	public ModelAndView asse_edit(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");

		List<Asstreport> s = asstreportrepost.getNetwt(id);

		ModelAndView modelAndView = new ModelAndView("asse_edit");

		model.addAttribute("check", s.get(0));

		return modelAndView;
	}

	@RequestMapping("/asse_report")
	public ModelAndView asse_report(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");

		List<Asstreport> s = asstreportrepost.getNetwt(id);

		ModelAndView modelAndView = new ModelAndView("asse_report");

		model.addAttribute("check", s.get(0));

		return modelAndView;
	}

	@RequestMapping("/item_summary")
	public ModelAndView FPO_ITEM(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");

		List<FPO_ITEM_DET> s = FPO_ITEMrepost.getitem(id);
		model.addAttribute("check", fpoService.getPojo(s.get(0)));

		String head = request.getParameter("id");
		List<FPO_MAIN> s1 = FPOrepost.getmain(head);
		fpoService.setFpoMainValues(s1);
		model.addAttribute("head", s1.get(0));

		ModelAndView modelAndView = new ModelAndView("item_summary");

		return modelAndView;
	}

	@RequestMapping(value = "/edit")
	public ModelAndView asse_report(@ModelAttribute("asse_report") Asstreport asse_report, Model model) {

		asstreportrepost.getNetwt1(asse_report.getNETWT(), asse_report.getITEM_DESC(), asse_report.getId());

		ModelAndView modelAndView = new ModelAndView("redirect:/asse_table");

		return modelAndView;
	}

	@RequestMapping(value = "/exam_order_add")
	public ModelAndView exam_order_add(@ModelAttribute("FPO_ORDER") FPO_ORDER FPO_ORDER, Model model) {

		java.util.Date utilDate = new java.util.Date();
		FPO_ORDER.setOFF_ID(offId);
		FPO_ORDER.setROLE(role);
		FPO_ORDER.setORDER_DATE(utilDate);
		FPO_ORDERrepost.save(FPO_ORDER);

		org.springframework.web.servlet.ModelAndView modelAndView = new ModelAndView(
				"redirect:/ead_submit?id=" + FPO_ORDER.getCIN_NO());
		;

		return modelAndView;
	}

	@RequestMapping("/process_ead")
	public String process_ead() {
		System.out.println("Process_ead invoker");
		return "process_ead";
	}

	@RequestMapping("/ead_roll")
	public String ead_roll() {
		System.out.println("EAD Roll invoker");
		return "ead_roll";
	}

	@RequestMapping("/import_roll")
	public String import_roll() {
		System.out.println("Import_roll invoker");
		return "import_roll";
	}

	@RequestMapping("/ead_list")
	public ModelAndView ead_list(Model model) {
		List<FPO_MAIN> s = FPOrepost.getview();
		ModelAndView modelAndView = new ModelAndView("ead_list");
		model.addAttribute("check", s);
		return modelAndView;
	}

	@RequestMapping("/import_list")
	public ModelAndView import_list(Model model) {
		List<FPO_MAIN> s = FPOrepost.getview();

		ModelAndView modelAndView = new ModelAndView("import_list");

		model.addAttribute("check", s);

		return modelAndView;
	}

	@RequestMapping("/ead_main")
	public ModelAndView ead_main(HttpServletRequest request, Model model) {
		ModelAndView modelAndView = new ModelAndView("ead_main");
		try {
			String id = request.getParameter("id");
			List<FPO_MAIN> s = FPOrepost.getmain(id);
			fpoService.setFpoMainValues(s);
			model.addAttribute("check", fpoService.getPojo(s.get(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return modelAndView;
	}

	@RequestMapping("/import_main")
	public ModelAndView import_main(HttpServletRequest request, Model model) {
		ModelAndView modelAndView = new ModelAndView("import_main");
		try {
			String id = request.getParameter("id");
			List<FPO_MAIN> s = FPOrepost.getmain(id);
			fpoService.setFpoMainValues(s);
			model.addAttribute("check", fpoService.getPojo(s.get(0)));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return modelAndView;
	}

	@RequestMapping("/ead_item")
	public ModelAndView ead_item(HttpServletRequest request, Model model) {
		try {
			String cinNo = request.getParameter("id");
			List<FPO_ITEM_DET> s = FPO_ITEMrepost.getitem(cinNo);
			List<FPO_ITEM_DET> ItemModYesNoOriginalList = new ArrayList<FPO_ITEM_DET>();
			List<FPO_ITEM_DET> ItemModAmendList = new ArrayList<FPO_ITEM_DET>();
			FPO_ITEM_DET fpoModelVal = new FPO_ITEM_DET();
			FPO_ITEM_DET fpoModel = new FPO_ITEM_DET();
			List<FPO_ITEM_DET> itemModYes = FPO_ITEMrepost.getItemByModYes(cinNo);
			List<FPO_ITEM_DET> itemModNo = FPO_ITEMrepost.getItemByModNo(cinNo);
			List<Object> genCth = FPO_ITEMrepost.getGENDROP();

			if ((null == itemModYes || itemModYes.isEmpty()) && (!(null == itemModNo) && !(itemModNo.isEmpty()))) {
				// No Amendment
				itemModNo.sort((FPO_ITEM_DET s1, FPO_ITEM_DET s2) -> s1.getITEM_NO().compareTo(s2.getITEM_NO()));
				model.addAttribute("check", fpoService.getListPojo(itemModNo));
				model.addAttribute("change", fpoService.getListPojo(s).get(0));

			} else {
				// Amendment
				for (FPO_ITEM_DET itmModNo : itemModNo) {
					ItemModYesNoOriginalList.add(itmModNo);
					fpoModelVal = new FPO_ITEM_DET();
					fpoModelVal.setITEM_NO(itmModNo.getITEM_NO());
					ItemModAmendList.add(fpoModelVal);
				}

				for (FPO_ITEM_DET itmModYes : itemModYes) {
					ItemModYesNoOriginalList.add(fpoService.getAmendByCinItem(cinNo, itmModYes.getITEM_NO()));
					ItemModAmendList.add(itmModYes);
				}

				if ((null == ItemModAmendList || ItemModAmendList.isEmpty())
						&& (null == ItemModYesNoOriginalList || ItemModYesNoOriginalList.isEmpty())) {
					model.addAttribute("check", s.get(0));
					model.addAttribute("change", s.get(0));
				} else {
					ItemModAmendList
							.sort((FPO_ITEM_DET s1, FPO_ITEM_DET s2) -> s1.getITEM_NO().compareTo(s2.getITEM_NO()));
					ItemModYesNoOriginalList
							.sort((FPO_ITEM_DET s1, FPO_ITEM_DET s2) -> s1.getITEM_NO().compareTo(s2.getITEM_NO()));
					itemModNo.sort((FPO_ITEM_DET s1, FPO_ITEM_DET s2) -> s1.getITEM_NO().compareTo(s2.getITEM_NO()));
					model.addAttribute("amend", fpoService.getListPojo(ItemModAmendList));
					model.addAttribute("check", fpoService.getListPojo(ItemModYesNoOriginalList));
					model.addAttribute("change", fpoService.getPojo(fpoModel));
				}

			}

			List<FPO_MAIN> s1 = FPOrepost.getmain(cinNo);
			if (!((s1 == null) || s1.isEmpty()))
				if (!(null == itemModYes) && !(itemModYes.isEmpty()))
					s1.get(0).setMODIFIED("Y");
				else
					s1.get(0).setMODIFIED("N");
			model.addAttribute("head", s1.get(0));
			model.addAttribute("genCth", genCth);
		} catch (Exception e) {

			e.printStackTrace();
		}
		ModelAndView modelAndView = new ModelAndView("ead_item");
		return modelAndView;
	}

	@RequestMapping("/declared")
	public ModelAndView declared(HttpServletRequest request, Model model)

	{
		try {
			String cinNo = request.getParameter("id");
			List<FPO_ITEM_DET> s = FPO_ITEMrepost.getitem(cinNo);
			List<FPO_ITEM_DET> ItemModYesNoOriginalList = new ArrayList<FPO_ITEM_DET>();
			List<FPO_ITEM_DET> ItemModAmendList = new ArrayList<FPO_ITEM_DET>();
			FPO_ITEM_DET fpoModelVal = new FPO_ITEM_DET();
			FPO_ITEM_DET fpoModel = new FPO_ITEM_DET();
			List<FPO_ITEM_DET> itemModYes = FPO_ITEMrepost.getItemByModYes(cinNo);
			List<FPO_ITEM_DET> itemModNo = FPO_ITEMrepost.getItemByModNo(cinNo);
			List<Object> genCth = FPO_ITEMrepost.getGENDROP();

			if ((null == itemModYes || itemModYes.isEmpty()) && (!(null == itemModNo) && !(itemModNo.isEmpty()))) {
				// No Amendment
				itemModNo.sort((FPO_ITEM_DET s1, FPO_ITEM_DET s2) -> s1.getITEM_NO().compareTo(s2.getITEM_NO()));
				model.addAttribute("check", fpoService.getListPojo(itemModNo));
				model.addAttribute("change", fpoService.getListPojo(s).get(0));

			} else {
				// Amendment
				for (FPO_ITEM_DET itmModNo : itemModNo) {
					ItemModYesNoOriginalList.add(itmModNo);
					fpoModelVal = new FPO_ITEM_DET();
					fpoModelVal.setITEM_NO(itmModNo.getITEM_NO());
					ItemModAmendList.add(fpoModelVal);
				}

				for (FPO_ITEM_DET itmModYes : itemModYes) {
					ItemModYesNoOriginalList.add(fpoService.getAmendByCinItem(cinNo, itmModYes.getITEM_NO()));
					ItemModAmendList.add(itmModYes);
				}

				if ((null == ItemModAmendList || ItemModAmendList.isEmpty())
						&& (null == ItemModYesNoOriginalList || ItemModYesNoOriginalList.isEmpty())) {
					model.addAttribute("check", s.get(0));
					model.addAttribute("change", s.get(0));
				} else {
					ItemModAmendList
							.sort((FPO_ITEM_DET s1, FPO_ITEM_DET s2) -> s1.getITEM_NO().compareTo(s2.getITEM_NO()));
					ItemModYesNoOriginalList
							.sort((FPO_ITEM_DET s1, FPO_ITEM_DET s2) -> s1.getITEM_NO().compareTo(s2.getITEM_NO()));
					itemModNo.sort((FPO_ITEM_DET s1, FPO_ITEM_DET s2) -> s1.getITEM_NO().compareTo(s2.getITEM_NO()));
					model.addAttribute("amend", fpoService.getListPojo(ItemModAmendList));
					model.addAttribute("check", fpoService.getListPojo(ItemModYesNoOriginalList));
					model.addAttribute("change", fpoService.getPojo(fpoModel));
				}

			}

			List<FPO_MAIN> s1 = FPOrepost.getmain(cinNo);
			if (!((s1 == null) || s1.isEmpty()))
				if (!(null == itemModYes) && !(itemModYes.isEmpty()))
					s1.get(0).setMODIFIED("Y");
				else
					s1.get(0).setMODIFIED("N");
			model.addAttribute("head", s1.get(0));
			model.addAttribute("genCth", genCth);
		} catch (Exception e) {

			e.printStackTrace();
		}
		ModelAndView modelAndView = new ModelAndView("declared");
		return modelAndView;
	}

	@RequestMapping("/eadItemUpdate")
	public ModelAndView eadItemUpdate(@ModelAttribute("fpo_item_det") FPO_ITEM_DET fpo_item_det,
			HttpServletRequest request, Model model) {
		java.util.Date utilDate = new java.util.Date();
		List<FPO_AMEND> fpoAmend = fpoService.getFlag(fpo_item_det.getCinNo());
		List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(fpo_item_det.getCinNo(),
				fpo_item_det.getITEM_NO());
		List<FPO_MAIN> s1 = FPOrepost.getmain(fpo_item_det.getCinNo());
		if (!((fpoAmend == null) || fpoAmend.isEmpty())) {
			fpoItemDET.get(0).setAMEND_SERAIL_NO(fpoAmend.size() + 1l);
			fpoItemDET.get(0).setOFF_ID(offId);
			fpoItemDET.get(0).setROLE(role);
			fpoService.moveItemToAmend(fpoItemDET.get(0));
			fpo_item_det.setMODIFIED("Y");
			fpo_item_det.setAMEND_FLAG("U");
			fpo_item_det.setId(fpoItemDET.get(0).getId());
			fpo_item_det.setAMEND_DT(utilDate);
			fpo_item_det.setAMEND_SERAIL_NO(fpoItemDET.get(0).getAMEND_SERAIL_NO());
			fpo_item_det.setOFF_ID(offId);
			fpo_item_det.setROLE(role);
			FPO_ITEMrepost.save(fpo_item_det);
		} else {
			// fpoItemDET = FPO_ITEMrepost.getitem(fpo_item_det.getCinNo());
			fpoItemDET.get(0).setAMEND_SERAIL_NO(fpo_item_det.getITEM_NO());
			fpoItemDET.get(0).setOFF_ID(offId);
			fpoItemDET.get(0).setROLE(role);
			fpoService.moveItemToAmend(fpoItemDET.get(0));
			fpo_item_det.setMODIFIED("Y");
			fpo_item_det.setAMEND_FLAG("U");
			fpo_item_det.setId(fpoItemDET.get(0).getId());
			fpo_item_det.setAMEND_DT(utilDate);
			fpo_item_det.setAMEND_SERAIL_NO(fpoItemDET.get(0).getAMEND_SERAIL_NO());
			fpo_item_det.setOFF_ID(offId);
			fpo_item_det.setROLE(role);
			FPO_ITEMrepost.save(fpo_item_det);
		}
		fpoAmend = fpoService.getFlag(fpo_item_det.getCinNo());
		model.addAttribute("check", fpoAmend.get(0));
		model.addAttribute("amend", fpoItemDET);
		model.addAttribute("head", s1.get(0));
		ModelAndView modelAndView = new ModelAndView("redirect:/ead_item?id=" + fpo_item_det.getCinNo());
		return modelAndView;
	}

	@RequestMapping("/addNewItem")
	public ModelAndView addNewItem(@ModelAttribute("fpo_item_det") FPO_ITEM_DET fpo_item_det,
			HttpServletRequest request, Model model) {
		java.util.Date utilDate = new java.util.Date();
		fpo_item_det.setAMEND_FLAG("A");
		fpo_item_det.setAMEND_DT(utilDate);
		fpo_item_det.setAMEND_SERAIL_NO(0l);
		fpo_item_det.setOFF_ID(offId);
		fpo_item_det.setROLE(role);
		fpo_item_det.setMODIFIED("N");
		fpo_item_det.setITEM_NO(FPO_ITEMrepost.getNoOfYesNoItems(fpo_item_det.getCinNo()) + 1l);
		FPO_ITEMrepost.save(fpo_item_det);
		List<FPO_AMEND> fpoAmend = fpoService.getFlag(fpo_item_det.getCinNo());
		List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getitem(fpo_item_det.getCinNo());
		List<FPO_MAIN> s1 = FPOrepost.getmain(fpo_item_det.getCinNo());

		if (!((fpoAmend == null) || fpoAmend.isEmpty()))
			model.addAttribute("check", fpoAmend.get(0));
		model.addAttribute("amend", fpoItemDET);
		model.addAttribute("head", s1.get(0));

		ModelAndView modelAndView = new ModelAndView("redirect:/ead_item?id=" + fpo_item_det.getCinNo());
		return modelAndView;
	}

	@RequestMapping(value = "/changeItem", produces = { MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public @ResponseBody FPO_ITEM_DET changeItem(@RequestBody FPO_ITEM_DET fPO_ITEM_DET, HttpServletRequest request,
			HttpServletResponse response, Model model) {

		String cinNo = request.getParameter("id");
		String itemNo = request.getParameter("itemNo");
		List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(cinNo, Long.valueOf(itemNo));
		model.addAttribute("check", fpoItemDET.get(0));
		return fpoItemDET.get(0);
	}

	@RequestMapping(value = "/addNewItemDetails", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public @ResponseBody FPO_ITEM_DET addNewItemDetails(@RequestBody FPO_ITEM_DET fPO_ITEM_DET,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		String cinNo = request.getParameter("id");
		String itemNo = request.getParameter("itemNo");
		List<FPO_AMEND> amend = fpoService.getFlag(cinNo);

		if (null == amend || amend.isEmpty()) {
			List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(cinNo, Long.valueOf(itemNo));
			return fpoItemDET.get(0);
		} else {
			/*
			 * fPO_ITEM_DET.setBCD_RTA(amend.get(0).getBCD_RTA());
			 * fPO_ITEM_DET.setIGST_RTA(amend.get(0).getIGST_RTA());
			 * fPO_ITEM_DET.setSW_RTA(amend.get(0).getSW_RTA());
			 */
			return fpoService.moveAmendToItem(amend.get(0));
		}
		// model.addAttribute("check", fpoItemDET.get(0));
		// return fPO_ITEM_DET;
	}

	@RequestMapping(value = "/getBcdNotification", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public @ResponseBody List<Object> getBcdNotification(@RequestBody FPO_ITEM_DET fPO_ITEM_DET,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		String cth = request.getParameter("cth");
		List<Object> getBcdNotification = FPO_ITEMrepost.getBcdNotification(cth);
		model.addAttribute("getBcdNotification", getBcdNotification);
		/*
		 * List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(cinNo,
		 * Long.valueOf(itemNo)); model.addAttribute("check", fpoItemDET.get(0));
		 */
		return getBcdNotification;
	}

	@RequestMapping(value = "/getBcdSerialNo", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public @ResponseBody List<Object> getBcdSerialNo(@RequestBody FPO_ITEM_DET fPO_ITEM_DET, HttpServletRequest request,
			HttpServletResponse response, Model model) {

		String notificationNo = request.getParameter("notificationNo");
		String cth = request.getParameter("cth");
		List<Object> getBcdSlNo = FPO_ITEMrepost.getBcdSlNo(notificationNo, cth);
		model.addAttribute("getBcdSlNo", getBcdSlNo);
		/*
		 * List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(cinNo,
		 * Long.valueOf(itemNo)); model.addAttribute("check", fpoItemDET.get(0));
		 */
		return getBcdSlNo;
	}

	@RequestMapping(value = "/getBcdRate", produces = { MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public @ResponseBody String getBcdRate(@RequestBody FPO_ITEM_DET fPO_ITEM_DET, HttpServletRequest request,
			HttpServletResponse response, Model model) {

		String getBcdSlNo = request.getParameter("getBcdSlNo");
		String cth = request.getParameter("cth");
		List<Object> getBcdRate = FPO_ITEMrepost.getBcdRate(getBcdSlNo, cth);
		model.addAttribute("getBcdRate", getBcdRate);
		/*
		 * List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(cinNo,
		 * Long.valueOf(itemNo)); model.addAttribute("check", fpoItemDET.get(0));
		 */
		return getBcdRate.get(0).toString();
	}

	@RequestMapping(value = "/getIgstNotification", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public @ResponseBody List<Object> getIgstNotification(@RequestBody FPO_ITEM_DET fPO_ITEM_DET,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		String cth = request.getParameter("cth");
		List<Object> getIgstNotification = FPO_ITEMrepost.getIgstNotification(cth);
		model.addAttribute("getIgstNotification", getIgstNotification);
		/*
		 * List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(cinNo,
		 * Long.valueOf(itemNo)); model.addAttribute("check", fpoItemDET.get(0));
		 */
		return getIgstNotification;
	}

	@RequestMapping(value = "/getIgstSerialNo", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public @ResponseBody List<Object> getIgstSerialNo(@RequestBody FPO_ITEM_DET fPO_ITEM_DET,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		String notificationNo = request.getParameter("notificationNo");
		String cth = request.getParameter("cth");
		List<Object> getIgstSerialNo = FPO_ITEMrepost.getIgstSerialNo(notificationNo, cth);
		model.addAttribute("getIgstSerialNo", getIgstSerialNo);
		/*
		 * List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(cinNo,
		 * Long.valueOf(itemNo)); model.addAttribute("check", fpoItemDET.get(0));
		 */
		return getIgstSerialNo;
	}

	@RequestMapping(value = "/getIgstRate", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public @ResponseBody String getIgstRate(@RequestBody FPO_ITEM_DET fPO_ITEM_DET, HttpServletRequest request,
			HttpServletResponse response, Model model) {

		String getSlNo = request.getParameter("getSlNo");
		String cth = request.getParameter("cth");
		List<Object> getIgstRate = FPO_ITEMrepost.getIgstRate(getSlNo, cth);
		model.addAttribute("getIgstRate", getIgstRate);
		/*
		 * List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(cinNo,
		 * Long.valueOf(itemNo)); model.addAttribute("check", fpoItemDET.get(0));
		 */
		return getIgstRate.get(0).toString();
	}

	@RequestMapping(value = "/getSwNotification", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public @ResponseBody List<Object> getSwNotification(@RequestBody FPO_ITEM_DET fPO_ITEM_DET,
			HttpServletRequest request, HttpServletResponse response, Model model) {

		String cth = request.getParameter("cth");
		List<Object> getSwNotification = FPO_ITEMrepost.getSwNotification(cth);
		model.addAttribute("getSwNotification", getSwNotification);
		/*
		 * List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(cinNo,
		 * Long.valueOf(itemNo)); model.addAttribute("check", fpoItemDET.get(0));
		 */
		return getSwNotification;
	}

	@RequestMapping(value = "/getSwSerialNo", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public @ResponseBody List<Object> getSwSerialNo(@RequestBody FPO_ITEM_DET fPO_ITEM_DET, HttpServletRequest request,
			HttpServletResponse response, Model model) {

		String notificationNo = request.getParameter("notificationNo");
		String cth = request.getParameter("cth");
		List<Object> getSwSerialNo = FPO_ITEMrepost.getSwSerialNo(notificationNo, cth);
		model.addAttribute("getSwSerialNo", getSwSerialNo);
		/*
		 * List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(cinNo,
		 * Long.valueOf(itemNo)); model.addAttribute("check", fpoItemDET.get(0));
		 */
		return getSwSerialNo;
	}

	@RequestMapping(value = "/getSwRate", produces = { MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public @ResponseBody String getSwRate(@RequestBody FPO_ITEM_DET fPO_ITEM_DET, HttpServletRequest request,
			HttpServletResponse response, Model model) {

		String getSlNo = request.getParameter("getSlNo");
		String cth = request.getParameter("cth");
		List<Object> getSwRate = FPO_ITEMrepost.getSwRate(getSlNo, cth);
		model.addAttribute("getSwRate", getSwRate);
		/*
		 * List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(cinNo,
		 * Long.valueOf(itemNo)); model.addAttribute("check", fpoItemDET.get(0));
		 */
		return getSwRate.get(0).toString();
	}

	@RequestMapping(value = "/getTotalNoItems", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public @ResponseBody String getTotalNoItems(HttpServletRequest request, HttpServletResponse response, Model model) {

		String cinNo = request.getParameter("cinNo");
		List<Object> getTotalNoItems = FPO_ITEMrepost.getTotalNoItems(cinNo);
		model.addAttribute("getTotalNoItems", getTotalNoItems);
		/*
		 * List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getItemByIdNo(cinNo,
		 * Long.valueOf(itemNo)); model.addAttribute("check", fpoItemDET.get(0));
		 */
		return getTotalNoItems.get(0).toString();
	}

	@RequestMapping("/deleteItem")
	public ModelAndView deleteItem(HttpServletRequest request, Model model) {
		String cinNo = request.getParameter("id");
		String itemNo = request.getParameter("itemNo");

		List<FPO_ITEM_DET> fpoItemDET = FPO_ITEMrepost.getitem(cinNo);

		if (fpoItemDET.size() > 1)
			FPO_ITEMrepost.deleteFpoItem("D", cinNo, itemNo);

		List<FPO_ITEM_DET> s = FPO_ITEMrepost.getitem(cinNo);
		if (!((s == null) || s.isEmpty()) && !(null == s.get(0).getMODIFIED() || s.get(0).getMODIFIED().isEmpty()))
			model.addAttribute("amend", s);
		else
			model.addAttribute("check", s.get(0));

		// fpoService.deleteAmendData(cinNo, itemNo);
		if (!model.containsAttribute("check")) {
			List<FPO_AMEND> s2 = fpoService.getFlag(cinNo);
			if (!((s2 == null) || s2.isEmpty()))
				model.addAttribute("check", s2.get(0));
		}

		List<FPO_MAIN> s1 = FPOrepost.getmain(cinNo);
		if (!((s1 == null) || s1.isEmpty()))
			s1.get(0).setMODIFIED(s.get(0).getMODIFIED());
		model.addAttribute("head", s1.get(0));

		ModelAndView modelAndView = new ModelAndView("redirect:/ead_item?id=" + cinNo);
		return modelAndView;
	}

	@RequestMapping("/import_item")
	public ModelAndView import_item(HttpServletRequest request, Model model) {
		String id = request.getParameter("id");
		List<FPO_ITEM_DET> s = FPO_ITEMrepost.getitem(id);
		model.addAttribute("check", s);

		String head = request.getParameter("id");
		List<FPO_MAIN> s1 = FPOrepost.getmain(head);
		fpoService.setFpoMainValues(s1);
		model.addAttribute("head", s1.get(0));

		ModelAndView modelAndView = new ModelAndView("import_item");

		return modelAndView;
	}

	@RequestMapping("/ead_submit")
	public ModelAndView ead_submit(HttpServletRequest request, Model model)

	{
		String cinNo = request.getParameter("id");
		// System.out.println("head:"+head);
		List<FPO_MAIN> s1 = FPOrepost.getmain(cinNo);
		fpoService.updateStatusOfAllPages(cinNo, s1.get(0).getITEM_ID(), "E3");
		fpoService.setFpoMainValues(s1);
		model.addAttribute("head", s1.get(0));

		List<DECI_REAS> s = DECI_REASrepost.getREAS();
		model.addAttribute("reason", s);

		ModelAndView modelAndView = new ModelAndView("ead_submit");
		return modelAndView;
	}

	@RequestMapping("/import_submit")
	public String import_submit() {
		System.out.println("Import Submit invoker");
		return "import_submit";
	}

	@RequestMapping("/ead_query")
	public ModelAndView ead_query(HttpServletRequest request, Model model) {
		String cinNo = request.getParameter("id");
		// System.out.println("head:"+head);
		List<FPO_MAIN> s1 = FPOrepost.getmain(cinNo);
		fpoService.updateStatusOfAllPages(cinNo, s1.get(0).getITEM_ID(), "E1");
		fpoService.setFpoMainValues(s1);
		model.addAttribute("head", s1.get(0));

		List<FPO_ITEM_DET> s = FPO_ITEMrepost.getitem(cinNo);
		model.addAttribute("itemEad", s);

		ModelAndView modelAndView = new ModelAndView("ead_query");
		return modelAndView;
	}

	@RequestMapping("/import_order")
	public ModelAndView import_order(HttpServletRequest request, Model model) {
		String head = request.getParameter("id");
		List<FPO_MAIN> s1 = FPOrepost.getmain(head);
		fpoService.setFpoMainValues(s1);
		model.addAttribute("head", s1.get(0));
		ModelAndView modelAndView = new ModelAndView("import_order");
		return modelAndView;
	}

	@RequestMapping("/decisionSubmit")
	public ModelAndView decisionSubmit(@ModelAttribute("fpoQueryDecision") FpoQueryDecision fpoQueryDecision,
			HttpServletRequest request, Model model) {
		List<FPO_MAIN> s1 = FPOrepost.getmain(fpoQueryDecision.getCIN_NO());
		model.addAttribute("check", s1);
		ModelAndView modelAndView = new ModelAndView(fpoService.decisionSubmitService(fpoQueryDecision));
		return modelAndView;
	}

	@RequestMapping("/decisionSubmitStatus")
	public @ResponseBody String decisionSubmitStatus(HttpServletRequest request) {
		String cinNo = request.getParameter("cinNo");
		Long queryNo = fpoQueryRepo.getMaxQueryNoOnCinNo(cinNo);
		if (null != queryNo)
			return queryNo.toString();
		return null;
	}

	@RequestMapping("/getPageStatus")
	public @ResponseBody String getPageStatus(HttpServletRequest request) {
		return fpoService.getStatusOfAllPages(request.getParameter("cinNo"));
	}

}