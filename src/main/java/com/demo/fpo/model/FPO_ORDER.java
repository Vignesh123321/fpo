package com.demo.fpo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "FPO_ORDER")
public class FPO_ORDER {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ORDER_SEQ")
	@SequenceGenerator(name = "ORDER_SEQ", sequenceName = "ORDER_SEQ", initialValue = 1)
	@Column(name = "ID")
	public Long id;

	@Column(name = "CIN_NO")
	private String CIN_NO;

	@Column(name = "CUS_SITE")
	private String CUS_SITE;

	@Column(name = "OFF_ID")
	private String OFF_ID;

	@Column(name = "ROLE")
	private String ROLE;

	@Column(name = "ORDER_DATE")
	private Date ORDER_DATE;

	@Column(name = "EXAM_ORDER")
	private String EXAM_ORDER;

	@Column(name = "ORDER_REMARK")
	private String ORDER_REMARK;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCIN_NO() {
		return CIN_NO;
	}

	public void setCIN_NO(String cIN_NO) {
		CIN_NO = cIN_NO;
	}

	public String getCUS_SITE() {
		return CUS_SITE;
	}

	public void setCUS_SITE(String cUS_SITE) {
		CUS_SITE = cUS_SITE;
	}

	public String getOFF_ID() {
		return OFF_ID;
	}

	public void setOFF_ID(String oFF_ID) {
		OFF_ID = oFF_ID;
	}

	public String getROLE() {
		return ROLE;
	}

	public void setROLE(String rOLE) {
		ROLE = rOLE;
	}

	public Date getORDER_DATE() {
		return ORDER_DATE;
	}

	public void setORDER_DATE(Date oRDER_DATE) {
		ORDER_DATE = oRDER_DATE;
	}

	public String getEXAM_ORDER() {
		return EXAM_ORDER;
	}

	public void setEXAM_ORDER(String eXAM_ORDER) {
		EXAM_ORDER = eXAM_ORDER;
	}

	public String getORDER_REMARK() {
		return ORDER_REMARK;
	}

	public void setORDER_REMARK(String oRDER_REMARK) {
		ORDER_REMARK = oRDER_REMARK;
	}

	
	
}
