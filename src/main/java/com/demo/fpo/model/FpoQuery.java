package com.demo.fpo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "FPO_QUERY")
public class FpoQuery {


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FPO_SEQ")
	@SequenceGenerator(name = "FPO_SEQ", sequenceName = "FPO_SEQ", initialValue = 1)
	@Column(name = "ID")
	public Long id;
	
	@Column(name = "CIN_NO")
	private String cinNo;

	@Column(name = "ITEM_ID")
	private String ITEM_ID;

	@Column(name = "CUS_SITE")
	private String CUS_SITE;
	
	@Column(name = "ITEM_NO")
	private Long ITEM_NO;
	
	@Column(name = "POSTINGDT")
	private String POSTINGDT;
	
	@Column(name = "QRY_NO")
	private Long QRY_NO;
	
	@Column(name = "QRY")
	private String QRY;
	
	@Column(name = "QRY_TYP")
	private String QRY_TYP;
	
	@Column(name = "QRY_DATE")
	private Date QRY_DATE;
	
	@Column(name = "QRY_OFF_ID")
	private String QRY_OFF_ID;
	
	@Column(name = "QRY_ROLE")
	private String QRY_ROLE;
	
	@Column(name = "RPLY")
	private String RPLY;
	
	@Column(name = "RPLY_OFF_ID")
	private String RPLY_OFF_ID;
	
	@Column(name = "RPLY_ROLE")
	private String RPLY_ROLE;
	
	@Column(name = "RPLY_DATE")
	private Date RPLY_DATE;
	
	@Column(name = "DEFUALT_QUERY")
	private String DEFUALT_QUERY;
	
	public String getDEFUALT_QUERY() {
		return DEFUALT_QUERY;
	}

	public void setDEFUALT_QUERY(String dEFUALT_QUERY) {
		DEFUALT_QUERY = dEFUALT_QUERY;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCinNo() {
		return cinNo;
	}

	public void setCinNo(String cinNo) {
		this.cinNo = cinNo;
	}

	public String getITEM_ID() {
		return ITEM_ID;
	}

	public void setITEM_ID(String iTEM_ID) {
		ITEM_ID = iTEM_ID;
	}

	public String getCUS_SITE() {
		return CUS_SITE;
	}

	public void setCUS_SITE(String cUS_SITE) {
		CUS_SITE = cUS_SITE;
	}

	public Long getITEM_NO() {
		return ITEM_NO;
	}

	public void setITEM_NO(Long iTEM_NO) {
		ITEM_NO = iTEM_NO;
	}

	public String getPOSTINGDT() {
		return POSTINGDT;
	}

	public void setPOSTINGDT(String pOSTINGDT) {
		POSTINGDT = pOSTINGDT;
	}

	public Long getQRY_NO() {
		return QRY_NO;
	}

	public void setQRY_NO(Long qRY_NO) {
		QRY_NO = qRY_NO;
	}

	public String getQRY() {
		return QRY;
	}

	public void setQRY(String qRY) {
		QRY = qRY;
	}

	public String getQRY_TYP() {
		return QRY_TYP;
	}

	public void setQRY_TYP(String qRY_TYP) {
		QRY_TYP = qRY_TYP;
	}

	public Date getQRY_DATE() {
		return QRY_DATE;
	}

	public void setQRY_DATE(Date qRY_DATE) {
		QRY_DATE = qRY_DATE;
	}

	public String getQRY_OFF_ID() {
		return QRY_OFF_ID;
	}

	public void setQRY_OFF_ID(String qRY_OFF_ID) {
		QRY_OFF_ID = qRY_OFF_ID;
	}

	public String getQRY_ROLE() {
		return QRY_ROLE;
	}

	public void setQRY_ROLE(String qRY_ROLE) {
		QRY_ROLE = qRY_ROLE;
	}

	public String getRPLY() {
		return RPLY;
	}

	public void setRPLY(String rPLY) {
		RPLY = rPLY;
	}

	public String getRPLY_OFF_ID() {
		return RPLY_OFF_ID;
	}

	public void setRPLY_OFF_ID(String rPLY_OFF_ID) {
		RPLY_OFF_ID = rPLY_OFF_ID;
	}

	public String getRPLY_ROLE() {
		return RPLY_ROLE;
	}

	public void setRPLY_ROLE(String rPLY_ROLE) {
		RPLY_ROLE = rPLY_ROLE;
	}

	public Date getRPLY_DATE() {
		return RPLY_DATE;
	}

	public void setRPLY_DATE(Date rPLY_DATE) {
		RPLY_DATE = rPLY_DATE;
	}
	
	
}
