package com.demo.fpo.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.fpo.model.FpoQueryDecision;

@Repository
public interface FpoQueryDecisionRepo extends JpaRepository<FpoQueryDecision, Long>{
	
	@Query(nativeQuery = true, value = "select QRY_TYPE from FPO_QRY_DECI where cin_no=? ORDER BY qry_dt  desc")
	List<String> getPageStatus(String cinNo);
}
