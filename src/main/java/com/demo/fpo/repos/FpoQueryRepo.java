package com.demo.fpo.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.fpo.model.FpoQuery;

@Repository
public interface FpoQueryRepo extends JpaRepository<FpoQuery, Long> {

	@Query(nativeQuery = true, value = "SELECT i.item_no,item_desc,cth,qry FROM FPO_QUERY q INNER JOIN FPO_ITEM_DET i ON  i.ITEM_NO = q.ITEM_NO  where i.CIN_NO = ?1 and q.qry_no = ?2")
	List<Object> getMemoData(String cinNo, Long queryNo);

	@Query(nativeQuery = true, value = "select max(qry_no) from fpo_query")
	Long getMaxQueryNo();
	
	@Query(nativeQuery = true, value = "select max(qry_no) from fpo_query where CIN_NO = ?1")
	Long getMaxQueryNoOnCinNo(String cinNo);
	
	@Query(nativeQuery = true, value = "select QRY  FROM FPO_QUERY where CIN_NO = ?1 and qry_no = ?2 and item_no is null ")
	String getRemarks(String cinNo, Long queryNo);
	
	@Query(nativeQuery = true, value = "select DEFUALT_QUERY  FROM FPO_QUERY where CIN_NO = ?1 and qry_no = ?2 and item_no is null ")
	String getDefualtQuery(String cinNo, Long queryNo);
}
