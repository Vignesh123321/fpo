package com.demo.fpo.repos;

import java.util.List;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.demo.fpo.model.FPO_ITEM_DET;

@Repository
public interface FPO_ITEM_DETrepost extends JpaRepository<FPO_ITEM_DET, Long> {

	@Query(nativeQuery = true, value = "SELECT * FROM FPO_ITEM_DET where CIN_NO = ?1 and (AMEND_FLAG is null or AMEND_FLAG != 'D') ORDER BY ITEM_NO")
	List<FPO_ITEM_DET> getitem(String cinNo);
	
	@Query(nativeQuery = true, value = "select COUNT(*) from fpo_item_det where cin_no = ?1 and (AMEND_FLAG is null or AMEND_FLAG != 'D') ORDER BY ITEM_NO")
	Long getNoOfYesNoItems(String cinNo);
	
	@Query(nativeQuery = true, value = "SELECT * FROM FPO_ITEM_DET where CIN_NO = ?1 and modified = 'Y' and (AMEND_FLAG is null or AMEND_FLAG != 'D') ORDER BY ITEM_NO")
	List<FPO_ITEM_DET> getItemByModYes(String cinNo);
	
	@Query(nativeQuery = true, value = "SELECT * FROM FPO_ITEM_DET where CIN_NO = ?1 and (modified is null or modified = 'N') and (AMEND_FLAG is null or AMEND_FLAG != 'D') ORDER BY ITEM_NO")
	List<FPO_ITEM_DET> getItemByModNo(String cinNo);
	
	@Query(nativeQuery = true, value = "SELECT * FROM FPO_ITEM_DET where CIN_NO = ?1 and ITEM_NO = ?2 and (AMEND_FLAG is null or AMEND_FLAG != 'D') ORDER BY ITEM_NO")
	List<FPO_ITEM_DET> getItemByIdNo(String cinNo, Long itemNo);
	
	@Query(nativeQuery=true,value="select distinct(CTH) from ops$dir.dt_notn_slno where notn_endt is null and substr(CTH,1,4)='9804'")
	List<Object> getGENDROP();
	
	@Query(nativeQuery=true,value="select DISTINCT(notn) from ops$dir.dt_notn_slno where notn_endt is null and cth = ?1 AND notn_type='C' and ad_flg is null")
	List<Object> getBcdNotification(String cth);
	
	@Query(nativeQuery=true,value="select DISTINCT(slno) from ops$dir.dt_notn_slno where notn_endt is null and cth = ?2 and notn = ?1 AND notn_type='C' and ad_flg is null")
	List<Object> getBcdSlNo(String notn,String cth);
	
	@Query(nativeQuery=true,value="select DISTINCT(rta) from ops$dir.dt_notn_slno where notn_endt is null and cth = ?2 and slno = ?1 AND notn_type='C' and ad_flg is null")
	List<Object> getBcdRate(String slno,String cth);
	
	@Query(nativeQuery=true,value="select DISTINCT(notn) from ops$dir.dt_notn_slno where notn_endt is null and notn_type='G' and cth = ?1 and ad_flg = 'I'")
	List<Object> getIgstNotification(String cth);
	
	@Query(nativeQuery=true,value="select DISTINCT(slno) from ops$dir.dt_notn_slno where notn_endt is null and notn_type='G' and cth = ?2 and notn = ?1 and ad_flg = 'I'")
	List<Object> getIgstSerialNo(String notn,String cth);
	
	@Query(nativeQuery=true,value="select DISTINCT(rta) from ops$dir.dt_notn_slno where notn_endt is null and notn_type='G' and cth = ?2 and slno = ?1 and ad_flg = 'I'")
	List<Object> getIgstRate(String slno,String cth);
	
	@Query(nativeQuery=true,value="select DISTINCT(notn) from ops$dir.dt_notn_slno where notn_endt is null and notn_type='C' and cth = ?1 and ad_flg = 'C'")
	List<Object> getSwNotification(String cth);
	
	@Query(nativeQuery=true,value="select DISTINCT(slno) from ops$dir.dt_notn_slno where notn_endt is null and notn_type='C' and cth = ?2 and notn = ?1 and ad_flg = 'C'")
	List<Object> getSwSerialNo(String notn,String cth);
	
	@Query(nativeQuery=true,value="select DISTINCT(rta) from ops$dir.dt_notn_slno where notn_endt is null and notn_type='C' and cth = ?2 and slno = ?1 and ad_flg = 'C'")
	List<Object> getSwRate(String slno,String cth);
	
	@Query(nativeQuery=true,value="select COUNT(ITEM_NO) from fpo_item_det where cin_no = ?1 and (AMEND_FLAG is null or AMEND_FLAG != 'D') ORDER BY ITEM_NO")
	List<Object> getTotalNoItems(String cin);

	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "UPDATE fpo_item_det SET ITEM_DESC=?2,NETWT=?1 where CIN_NO = ?3")
	void updateFpoItemDesc(float netwt, String itemdesc, String cinNo);

	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "UPDATE fpo_item_det SET amend_flag = ?1 WHERE cin_no = ?2 and item_no = ?3")
	void deleteFpoItem(String amendFlag, String cinNo, String itemNo);

}