package com.demo.fpo.repos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.fpo.model.FPO_MAIN;

@Repository
public interface FPO_MAINrepost extends JpaRepository<FPO_MAIN, Long> {

	@Query(nativeQuery = true, value = "SELECT * FROM FPO_MAIN")
	List<FPO_MAIN> getview();

	@Query(nativeQuery = true, value = "SELECT * FROM FPO_MAIN where CIN_NO = ?1")
	List<FPO_MAIN> getmain(String id);
	
	@Query(nativeQuery = true, value = "SELECT f.interpretation FROM ops$dir.PDI_HANDLING_CLASS_CODES f INNER JOIN FPO_MAIN qa ON f.CODE = qa.HANDLING_CLASS_CD where CIN_NO = ?1")
	String getShortValue(String id);
	
	@Query(nativeQuery = true, value = "SELECT f.interpretation FROM ops$dir.PDI_NATURE_TRANS_CODES f INNER JOIN FPO_MAIN qa ON f.CODE = qa.NATURE_TYPE_CD where CIN_NO = ?1")
	String getNatureValue(String id);	
	
	
	
}
