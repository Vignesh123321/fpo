package com.demo.fpo.repos;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.demo.fpo.model.FPO_ORDER;

@Repository
public interface FPO_ORDERrepost extends JpaRepository<FPO_ORDER, Long> {
	
	/*
	 * @Modifying
	 * 
	 * @Transactional
	 * 
	 * @Query(nativeQuery=true,value="INSERT into FPO_ORDER values ('CIN_NO=?1')")
	 * void getORDER(String CIN_NO);
	 */
	
	@Query(nativeQuery = true, value = "select count(*) from fpo_query where CIN_NO = ?1")
	Long getOrderCinNo(String cinNo);
	
}
 








