package com.demo.fpo.repos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.fpo.model.FPO_AMEND;

@Repository
public interface FPO_AMENDrepost extends JpaRepository<FPO_AMEND, Long> {

	@Query(nativeQuery = true, value = "SELECT * from FPO_AMEND_ITEM_DET where CIN_NO = ?1 order by AMEND_SERIAL_NO")
	List<FPO_AMEND> getFpoAmend(String cinNo);

	@Query(nativeQuery = true, value = "SELECT * from FPO_AMEND_ITEM_DET where CIN_NO = ?1 and item_no = ?2 order by AMEND_SERIAL_NO")
	List<FPO_AMEND> getAmendByCinItem(String cinNo, Long itemNo);

	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "UPDATE FPO_AMEND_ITEM_DET SET amend_flag = ?1 WHERE cin_no = ?2 and item_no = ?3")
	void deleteFpoItem(String amendFlag, String cinNo, String itemNo);

	/*
	 * @Modifying
	 * 
	 * @Transactional public static List<FPO_AMEND>
	 * findBooksByAuthorNameAndTitle(String authorName, EntityManager entityManager)
	 * { CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	 * CriteriaQuery<FPO_AMEND> cq = cb.createQuery(FPO_AMEND.class);
	 * 
	 * Root<FPO_AMEND> fpoAmend = cq.from(FPO_AMEND.class); Predicate cinNo =
	 * cb.equal(fpoAmend.get("CIN_NO"), authorName); cq.where(cinNo);
	 * 
	 * Root<FPO_AMEND> fpoAmend = cq.from(FPO_AMEND.class); TypedQuery<FPO_AMEND>
	 * query = entityManager.createQuery(cq); return query.getResultList(); }
	 */

}
