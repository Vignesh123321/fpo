package com.demo.fpo.repos;

import java.util.List;

import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.demo.fpo.model.Asstreport;

@Repository

public interface Asstreportrepost extends JpaRepository<Asstreport, Long>{
	@Query(nativeQuery=true,value="SELECT * FROM fpo_item_det")
	List<Asstreport> getNetwt();
	
	@Query(nativeQuery=true,value="SELECT * FROM fpo_item_det where CIN_NO = ?1")
	List<Asstreport> getNetwt(String id);
	
	@Modifying
	@Transactional
	@Query(nativeQuery=true,value="UPDATE fpo_item_det SET ITEM_DESC=?2,NETWT=?1 where CIN_NO = ?3")
	void getNetwt1(float netwt, String itemdesc, String id); 

}
