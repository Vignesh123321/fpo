package com.demo.fpo.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.demo.fpo.model.FpoDefualtQuery;

@Repository
public interface FpoDefualtQueryRepo extends JpaRepository<FpoDefualtQuery, Long>{
	
}
