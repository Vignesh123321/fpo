package com.demo.fpo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.demo.fpo.controller.LoginController;
import com.demo.fpo.model.C_CUSITM;
import com.demo.fpo.model.FPO_AMEND;
import com.demo.fpo.model.FPO_ITEM_DET;
import com.demo.fpo.model.FPO_MAIN;
import com.demo.fpo.model.FpoDefualtQuery;
import com.demo.fpo.model.FpoQuery;
import com.demo.fpo.model.FpoQueryDecision;
import com.demo.fpo.repos.FPO_AMENDrepost;
import com.demo.fpo.repos.FPO_ITEM_DETrepost;
import com.demo.fpo.repos.FPO_MAINrepost;
import com.demo.fpo.repos.FPO_ORDERrepost;
import com.demo.fpo.repos.FpoQueryDecisionRepo;
import com.demo.fpo.repos.FpoQueryRepo;

@Service
@Component
public class FpoService {

	@Autowired
	FPO_AMENDrepost fpoAmendRpo;

	@Autowired
	FpoQueryRepo fpoQueryRepo;

	@Autowired
	FPO_ORDERrepost fpoOrderRepost;

	public List<FPO_AMEND> getFlag(String cinNo) {
		return fpoAmendRpo.getFpoAmend(cinNo);
	}

	public FPO_ITEM_DET getAmendByCinItem(String cinNo, Long itemNo) {
		return moveAmendToItem(fpoAmendRpo.getAmendByCinItem(cinNo, itemNo).get(0));
	}

	public void deleteAmendData(String cinNo, String itemNo) {
		fpoAmendRpo.deleteFpoItem("D", cinNo, itemNo);
	}

	@Autowired
	FpoQueryDecisionRepo fpoQueryDecisionRepo;

	public String decisionSubmitService(FpoQueryDecision fpoQueryDecision) {
		Long queryNo = fpoQueryRepo.getMaxQueryNoOnCinNo(fpoQueryDecision.getCIN_NO());
		fpoQueryDecision.setQRY_NO(queryNo);
		fpoQueryDecision.setOFF_ID(LoginController.offId);
		fpoQueryDecision.setROLE(LoginController.role);
		fpoQueryDecision.setCUS_SITE(LoginController.cuSite);
		fpoQueryDecisionRepo.save(fpoQueryDecision);
		Long orderNo = fpoOrderRepost.getOrderCinNo(fpoQueryDecision.getCIN_NO());
		return reDirectToPage(fpoQueryDecision.getDECI_CD(), queryNo, orderNo);
	}

	public void updateStatusOfAllPages(String cinNo, String itemId, String qryType) {
		FpoQueryDecision fpoQueryDecision = new FpoQueryDecision();

		fpoQueryDecision.setCIN_NO(cinNo);
		fpoQueryDecision.setITEM_ID(itemId);
		fpoQueryDecision.setQRY_TYPE(qryType);
		fpoQueryDecision.setOFF_ID(LoginController.offId);
		fpoQueryDecision.setROLE(LoginController.role);
		fpoQueryDecision.setCUS_SITE(LoginController.cuSite);
		fpoQueryDecisionRepo.save(fpoQueryDecision);
	}

	public String getStatusOfAllPages(String cinNo) {
		List<String> pageStatus = fpoQueryDecisionRepo.getPageStatus(cinNo);

		if (pageStatus.get(0).equals("E1"))
			return "order";

		if (pageStatus.get(0).equals("E2"))
			return "ead_query";

		if (pageStatus.get(0).equals("E3"))
			return "ead_submit";
		return "ead_item";
	}

	private String reDirectToPage(Long fpoQueryDecision, Long queryNo, Long orderNo) {
		return "main";
	}

	@Autowired
	FPO_MAINrepost FPOrepost;

	public List<FPO_MAIN> setFpoMainValues(List<FPO_MAIN> fpoMain) {

		if (null != fpoMain.get(0).getHANDLING_CLASS_CD()) {
			String shortValue = FPOrepost.getShortValue(fpoMain.get(0).getId());
			fpoMain.get(0).setHANDLING_CLASS_CD(fpoMain.get(0).getHANDLING_CLASS_CD() + "-" + shortValue);
		}

		if (null != fpoMain.get(0).getNATURE_TYPE_CD()) {
			String shortValue = FPOrepost.getNatureValue(fpoMain.get(0).getId());
			fpoMain.get(0).setNATURE_TYPE_CD(shortValue);
		}

		return fpoMain;
	}

	@Autowired
	FPO_ITEM_DETrepost fpoItemRepost;

	public void insertFpoQuery(FPO_ITEM_DET fpoItemDet) {
		int index;
		List<FpoQuery> fpoQueryRepoList = new ArrayList<FpoQuery>();
		List<FPO_ITEM_DET> fpoItemList = fpoItemRepost.getitem(fpoItemDet.getCinNo());
		String fpoQueryData = fpoItemDet.getQuery();
		String[] fpoQueryList = fpoQueryData.split(",");
		Long maxQueryNo = fpoQueryRepo.getMaxQueryNo();
		if (null == maxQueryNo || maxQueryNo.toString().isEmpty())
			maxQueryNo = 0l;
		for (FPO_ITEM_DET fpoItem : fpoItemList) {
			FpoQuery fpoQuery = new FpoQuery();
			fpoQuery.setCinNo(fpoItem.getCinNo());
			fpoQuery.setCUS_SITE(fpoItem.getCUS_SITE());
			fpoQuery.setITEM_ID(fpoItem.getITEM_ID());
			fpoQuery.setITEM_NO(fpoItem.getITEM_NO());
			fpoQuery.setPOSTINGDT(fpoItem.getPOSTINGDT());
			index = fpoItem.getITEM_NO().intValue();
			fpoQuery.setQRY_DATE(new Date());
			fpoQuery.setQRY_OFF_ID(fpoItem.getOFF_ID());
			fpoQuery.setQRY_ROLE(fpoItem.getROLE());
			fpoQuery.setQRY_TYP("E");
			fpoQuery.setQRY_NO(maxQueryNo + 1l);
			fpoQuery.setCUS_SITE(LoginController.cuSite);
			if (index > fpoQueryList.length) {
				fpoQuery.setQRY("");
			} else if (!(null == fpoQueryList[index - 1]) && !(fpoQueryList[index - 1].isEmpty())) {
				fpoQuery.setQRY(fpoQueryList[index - 1]);
				fpoQueryRepoList.add(fpoQuery);
			}

		}
		FpoQuery fpoQueryRemarks = new FpoQuery();
		fpoQueryRemarks.setCinNo(fpoItemDet.getCinNo());
		fpoQueryRemarks.setCUS_SITE(fpoItemDet.getCUS_SITE());
		fpoQueryRemarks.setITEM_ID(fpoItemDet.getITEM_ID());
		fpoQueryRemarks.setPOSTINGDT(fpoItemDet.getPOSTINGDT());
		fpoQueryRemarks.setDEFUALT_QUERY(fpoItemDet.getDefualtQueryOne() + "," + fpoItemDet.getDefualtQueryTwo() + ","
				+ fpoItemDet.getDefualtQueryThree());
		fpoQueryRemarks.setQRY_DATE(new Date());
		fpoQueryRemarks.setQRY_OFF_ID(LoginController.offId);
		fpoQueryRemarks.setQRY_ROLE(LoginController.role);
		fpoQueryRemarks.setQRY_TYP("E");
		fpoQueryRemarks.setQRY_NO(maxQueryNo + 1l);
		fpoQueryRemarks.setCUS_SITE(LoginController.cuSite);
		fpoQueryRemarks.setQRY(fpoItemDet.getQueryRemarks());
		fpoQueryRepoList.add(fpoQueryRemarks);
		fpoQueryRepo.saveAll(fpoQueryRepoList);
	}

	@Autowired
	com.demo.fpo.repos.FpoDefualtQueryRepo fpoDefualtQueryRepo;

	public List<String> getDefualtQuery() {
		List<String> listQuery = new ArrayList<String>();
		List<FpoDefualtQuery> fpoDefualtQueryList = fpoDefualtQueryRepo.findAll();
		for (FpoDefualtQuery fpoDefualtQuery : fpoDefualtQueryList) {
			listQuery.add(fpoDefualtQuery.getDEFAULT_QUERY());
		}
		return listQuery;
	}

	public List<String> getSpecifiedDefualtQuery(String serilNo) {

		String[] arrySerialNo = serilNo.split(",");
		List<String> listQuery = new ArrayList<String>();
		List<FpoDefualtQuery> fpoDefualtQueryList = fpoDefualtQueryRepo.findAll();
		for (FpoDefualtQuery fpoDefualtQuery : fpoDefualtQueryList) {
			if (null != arrySerialNo[fpoDefualtQuery.getSERIAL_NO().intValue() - 1]
					&& !(arrySerialNo[fpoDefualtQuery.getSERIAL_NO().intValue() - 1].toString().isEmpty()))
				if (fpoDefualtQuery.getSERIAL_NO().toString()
						.equals(arrySerialNo[fpoDefualtQuery.getSERIAL_NO().intValue() - 1]))
					listQuery.add(fpoDefualtQuery.getDEFAULT_QUERY());
		}
		return listQuery;
	}

	public FPO_ITEM_DET moveAmendToItem(FPO_AMEND fpoAmendEntity) {
		FPO_ITEM_DET fpoItemDet = new FPO_ITEM_DET();
		fpoItemDet.setAMEND_DT(fpoAmendEntity.getAMEND_DT());
		fpoItemDet.setAMEND_FLAG(fpoAmendEntity.getAMEND_FLAG());
		fpoItemDet.setAMEND_SERAIL_NO(fpoAmendEntity.getAMEND_SERIAL_NO());
		fpoItemDet.setBCD_AMT(fpoAmendEntity.getBCD_AMT());
		fpoItemDet.setBCD_AMT_FG(fpoAmendEntity.getBCD_AMT_FG());
		fpoItemDet.setBCD_NOTN(fpoAmendEntity.getBCD_NOTN());
		fpoItemDet.setBCD_NSNO(fpoAmendEntity.getBCD_NSNO());
		fpoItemDet.setBCD_RTA(fpoAmendEntity.getBCD_RTA());
		fpoItemDet.setCIN_DT(fpoAmendEntity.getAMEND_DT());
		fpoItemDet.setCinNo(fpoAmendEntity.getCIN_NO());
		fpoItemDet.setCTH(fpoAmendEntity.getCTH());
		fpoItemDet.setCTH_REVISED(fpoAmendEntity.getCTH());
		fpoItemDet.setCURRCD(fpoAmendEntity.getCURRCD());
		fpoItemDet.setCUS_SITE(fpoAmendEntity.getCUS_SITE());
		fpoItemDet.setDECL_VAL(fpoAmendEntity.getDECL_VAL());
		fpoItemDet.setDUTY(fpoAmendEntity.getDUTY());
		fpoItemDet.setDUTY_FG(fpoAmendEntity.getDUTY_FG());
		fpoItemDet.setFREIGHT_VAL(fpoAmendEntity.getFREIGHT_VAL());
		fpoItemDet.setGEN_CTH(fpoAmendEntity.getCTH());
		fpoItemDet.setIGST_AMT(fpoAmendEntity.getIGST_AMT());
		fpoItemDet.setIGST_AMT_FG(fpoAmendEntity.getIGST_AMT_FG());
		fpoItemDet.setIGST_NOTN(fpoAmendEntity.getIGST_NOTN());
		fpoItemDet.setIGST_NSNO(fpoAmendEntity.getIGST_NSNO());
		fpoItemDet.setIGST_RTA(fpoAmendEntity.getIGST_RTA());
		fpoItemDet.setINSURED_VAL(fpoAmendEntity.getINSURED_VAL());
		fpoItemDet.setITEM_DESC(fpoAmendEntity.getITEM_DESC());
		fpoItemDet.setITEM_ID(fpoAmendEntity.getITEM_ID());
		fpoItemDet.setITEM_NO(fpoAmendEntity.getITEM_NO());
		fpoItemDet.setITEM_REVISEDDESC(fpoAmendEntity.getITEM_DESC());
		// fpoItemDet.setJOB_DT(fpoAmendEntity.getD);
		// fpoItemDet.setJOB_NO(jOB_NO);
		fpoItemDet.setMESG_TYPE_CD(fpoAmendEntity.getMESG_TYPE_CD());
		fpoItemDet.setMODIFIED(fpoAmendEntity.getMODIFIED());
		fpoItemDet.setNETWT(fpoAmendEntity.getNETWT());
		fpoItemDet.setNOU(fpoAmendEntity.getNOU());
		fpoItemDet.setOFF_ID(fpoAmendEntity.getOFF_ID());
		fpoItemDet.setORIGCNTRYCD(fpoAmendEntity.getORIGCNTRYCD());
		fpoItemDet.setPOSTINGDT(fpoAmendEntity.getPOSTINGDT());
		fpoItemDet.setRATE(fpoAmendEntity.getRATE());
		fpoItemDet.setROLE(fpoAmendEntity.getROLE());
		fpoItemDet.setSW_AMT(fpoAmendEntity.getSW_AMT());
		fpoItemDet.setSW_AMT_FG(fpoAmendEntity.getSW_AMT_FG());
		fpoItemDet.setSW_NOTN(fpoAmendEntity.getSW_NOTN());
		fpoItemDet.setSW_NSNO(fpoAmendEntity.getSW_NSNO());
		fpoItemDet.setSW_RTA(fpoAmendEntity.getSW_RTA());
		fpoItemDet.setUNIQUE_ID(fpoAmendEntity.getUNIQUE_ID());
		return fpoItemDet;
	}

	public void moveItemToAmend(FPO_ITEM_DET fpoItemDet) {
		java.util.Date utilDate = new java.util.Date();

		FPO_AMEND fpoAmendEntity = new FPO_AMEND();
		fpoAmendEntity.setAMEND_SERIAL_NO(fpoItemDet.getAMEND_SERAIL_NO());
		fpoAmendEntity.setAMEND_DT(utilDate);
		fpoAmendEntity.setAMEND_FLAG("U");
		fpoAmendEntity.setBCD_AMT(fpoItemDet.getBCD_AMT());
		fpoAmendEntity.setBCD_AMT_FG(fpoItemDet.getBCD_AMT_FG());
		fpoAmendEntity.setBCD_NOTN(fpoItemDet.getBCD_NOTN());
		fpoAmendEntity.setBCD_NSNO(fpoItemDet.getBCD_NSNO());
		fpoAmendEntity.setBCD_RTA(fpoItemDet.getBCD_RTA());
		fpoAmendEntity.setCIN_NO(fpoItemDet.getCinNo());
		fpoAmendEntity.setCTH(fpoItemDet.getCTH());
		fpoAmendEntity.setCURR_RATE(fpoItemDet.getRATE());
		fpoAmendEntity.setCURRCD(fpoItemDet.getCURRCD());
		fpoAmendEntity.setCUS_SITE(fpoItemDet.getCUS_SITE());
		fpoAmendEntity.setDECL_VAL(fpoItemDet.getDECL_VAL());
		fpoAmendEntity.setDUTY(fpoItemDet.getDUTY());
		fpoAmendEntity.setDUTY_FG(fpoItemDet.getDUTY_FG());
		fpoAmendEntity.setFREIGHT_VAL(fpoItemDet.getFREIGHT_VAL());
		fpoAmendEntity.setIGST_AMT_FG(fpoItemDet.getIGST_AMT());
		// fpoAmendEntity.setId(fpoItemDet.getId());
		fpoAmendEntity.setIGST_AMT(fpoItemDet.getIGST_AMT());
		fpoAmendEntity.setIGST_NOTN(fpoItemDet.getIGST_NOTN());
		fpoAmendEntity.setIGST_NSNO(fpoItemDet.getIGST_NSNO());
		fpoAmendEntity.setIGST_RTA(fpoItemDet.getIGST_RTA());
		fpoAmendEntity.setINSURED_VAL(fpoItemDet.getINSURED_VAL());
		fpoAmendEntity.setITEM_DESC("MEN");
		fpoAmendEntity.setITEM_ID(fpoItemDet.getITEM_ID());
		fpoAmendEntity.setITEM_NO(fpoItemDet.getITEM_NO());
		fpoAmendEntity.setITEM_UNIQUE(1l);
		fpoAmendEntity.setMESG_TYPE_CD(fpoItemDet.getMESG_TYPE_CD());
		fpoAmendEntity.setMODIFIED("Y");// fpoItemDet.getMODIFIED()
		fpoAmendEntity.setNETWT(fpoItemDet.getNETWT());
		fpoAmendEntity.setNOU(fpoItemDet.getNOU());
		fpoAmendEntity.setOFF_ID(fpoItemDet.getOFF_ID());
		fpoAmendEntity.setRATE(fpoItemDet.getRATE());
		fpoAmendEntity.setORIGCNTRYCD(fpoItemDet.getORIGCNTRYCD());
		fpoAmendEntity.setPOSTINGDT(fpoItemDet.getPOSTINGDT());
		fpoAmendEntity.setROLE(fpoItemDet.getROLE());
		fpoAmendEntity.setSW_AMT(fpoItemDet.getSW_AMT());
		fpoAmendEntity.setSW_AMT_FG(fpoItemDet.getSW_AMT_FG());
		fpoAmendEntity.setSW_NOTN(fpoItemDet.getSW_NOTN());
		fpoAmendEntity.setSW_NSNO(fpoItemDet.getSW_NSNO());
		fpoAmendEntity.setSW_RTA(fpoItemDet.getSW_RTA());
		fpoAmendEntity.setUNIQUE_ID(fpoItemDet.getUNIQUE_ID());
		fpoAmendRpo.save(fpoAmendEntity);

	}

	public FPO_MAIN getPojo(FPO_MAIN t) {

		if (null == t.getCUS_SITE() || t.getCUS_SITE().isEmpty())
			t.setCUS_SITE("- NIL -");

		if (null == t.getCUST_ORG_CD() || t.getCUST_ORG_CD().isEmpty())
			t.setCUST_ORG_CD("- NIL -");

		if (null == t.getDECLARATION_PID() || t.getDECLARATION_PID().isEmpty())
			t.setDECLARATION_PID("- NIL -");

		if (null == t.getDECLARATION_PID() || t.getDECLARATION_PID().isEmpty())
			t.setDECLARATION_PID("- NIL -");

		if (null == t.getDESTPOST_ORG_CD() || t.getDESTPOST_ORG_CD().isEmpty())
			t.setDESTPOST_ORG_CD("- NIL -");

		if (null == t.getDOCUMENTS() || t.getDOCUMENTS().isEmpty())
			t.setDOCUMENTS("- NIL -");

		if (null == t.getFPO_CODE() || t.getFPO_CODE().isEmpty())
			t.setFPO_CODE("- NIL -");

		if (null == t.getGROSS_WT().toString() || t.getGROSS_WT().toString().isEmpty())
			t.setGROSS_WT(0f);

		if (null == t.getHANDLING_CLASS_CD() || t.getHANDLING_CLASS_CD().isEmpty())
			t.setHANDLING_CLASS_CD("- NIL -");

		if (null == t.getId() || t.getId().isEmpty())
			t.setId("- NIL -");

		if (null == t.getINS_VAL() || t.getINS_VAL().toString().isEmpty())
			t.setINS_VAL(0l);

		if (null == t.getINS_VAL_CURRCD() || t.getINS_VAL_CURRCD().isEmpty())
			t.setINS_VAL_CURRCD("- NIL -");

		if (null == t.getINTERPRETATION() || t.getINTERPRETATION().isEmpty())
			t.setINTERPRETATION("- NIL -");

		if (null == t.getITEM_ID() || t.getITEM_ID().isEmpty())
			t.setITEM_ID("- NIL -");

		if (null == t.getJOB_NO() || t.getJOB_NO().toString().isEmpty())
			t.setJOB_NO(0l);

		if (null == t.getLOCALID() || t.getLOCALID().isEmpty())
			t.setLOCALID("- NIL -");

		if (null == t.getLOCALID2() || t.getLOCALID2().isEmpty())
			t.setLOCALID2("- NIL -");

		if (null == t.getMAIL_CATEGORY_CD() || t.getMAIL_CATEGORY_CD().isEmpty())
			t.setMAIL_CATEGORY_CD("- NIL -");

		if (null == t.getMAIL_CLASS_CD() || t.getMAIL_CLASS_CD().isEmpty())
			t.setMAIL_CLASS_CD("- NIL -");

		if (null == t.getMAIL_PID() || t.getMAIL_PID().isEmpty())
			t.setMAIL_PID("- NIL -");

		if (null == t.getMAIL_STATE_CD() || t.getMAIL_STATE_CD().isEmpty())
			t.setMAIL_STATE_CD("- NIL -");

		if (null == t.getMAIL_STATE_REMARKS() || t.getMAIL_STATE_REMARKS().isEmpty())
			t.setMAIL_STATE_REMARKS("- NIL -");

		if (null == t.getMEASURED_GROSS_WT() || t.getMEASURED_GROSS_WT().toString().isEmpty())
			t.setMEASURED_GROSS_WT(0l);

		if (null == t.getMESG_TYPE_CD() || t.getMESG_TYPE_CD().isEmpty())
			t.setMESG_TYPE_CD("- NIL -");

		if (null == t.getMODIFIED() || t.getMODIFIED().isEmpty())
			t.setMODIFIED("- NIL -");

		if (null == t.getNATURE_TYPE_CD() || t.getNATURE_TYPE_CD().isEmpty())
			t.setNATURE_TYPE_CD("- NIL -");

		if (null == t.getORIGPOST_ORGCD() || t.getORIGPOST_ORGCD().isEmpty())
			t.setORIGPOST_ORGCD("- NIL -");

		if (null == t.getPINCODE() || t.getPINCODE().isEmpty())
			t.setPINCODE("- NIL -");

		if (null == t.getPOST_ORG_CD() || t.getPOST_ORG_CD().isEmpty())
			t.setPOST_ORG_CD("- NIL -");

		if (null == t.getPOSTAGE_AMT() || t.getPOSTAGE_AMT().toString().isEmpty())
			t.setPOSTAGE_AMT(0l);

		if (null == t.getPOSTAGE_CURR_CD() || t.getPOSTAGE_CURR_CD().isEmpty())
			t.setPOSTAGE_CURR_CD("- NIL -");

		if (null == t.getPOSTINGDT() || t.getPOSTINGDT().isEmpty())
			t.setPOSTINGDT("- NIL -");

		if (null == t.getRECP_ADD1() || t.getRECP_ADD1().isEmpty())
			t.setRECP_ADD1("- NIL -");

		if (null == t.getRECP_ADD2() || t.getRECP_ADD2().isEmpty())
			t.setRECP_ADD2("- NIL -");

		if (null == t.getRECP_CITY() || t.getRECP_CITY().isEmpty())
			t.setRECP_CITY("- NIL -");

		if (null == t.getRECP_CNTRY_CD() || t.getRECP_CNTRY_CD().isEmpty())
			t.setRECP_CNTRY_CD("- NIL -");

		if (null == t.getRECP_EMAILID() || t.getRECP_EMAILID().isEmpty())
			t.setRECP_EMAILID("- NIL -");

		if (null == t.getRECP_FAX() || t.getRECP_FAX().isEmpty())
			t.setRECP_FAX("- NIL -");

		if (null == t.getRECP_FNAME() || t.getRECP_FNAME().isEmpty())
			t.setRECP_FNAME("- NIL -");

		if (null == t.getRECP_IDREF() || t.getRECP_IDREF().isEmpty())
			t.setRECP_IDREF("- NIL -");

		if (null == t.getRECP_LNAME() || t.getRECP_LNAME().isEmpty())
			t.setRECP_LNAME("- NIL -");

		if (null == t.getRECP_NAME() || t.getRECP_NAME().isEmpty())
			t.setRECP_NAME("- NIL -");

		if (null == t.getRECP_PHONE() || t.getRECP_PHONE().isEmpty())
			t.setRECP_PHONE("- NIL -");

		if (null == t.getRECP_STATE() || t.getRECP_STATE().isEmpty())
			t.setRECP_STATE("- NIL -");

		if (null == t.getRECP_ZIP() || t.getRECP_ZIP().isEmpty())
			t.setRECP_ZIP("- NIL -");

		if (null == t.getSEND_ADD1() || t.getSEND_ADD1().isEmpty())
			t.setSEND_ADD1("- NIL -");

		if (null == t.getSEND_ADD2() || t.getSEND_ADD2().isEmpty())
			t.setSEND_ADD2("- NIL -");

		if (null == t.getSEND_CITY() || t.getSEND_CITY().isEmpty())
			t.setSEND_CITY("- NIL -");

		if (null == t.getSEND_CNTRY_CD() || t.getSEND_CNTRY_CD().isEmpty())
			t.setSEND_CNTRY_CD("- NIL -");

		if (null == t.getSEND_EMAILID() || t.getSEND_EMAILID().isEmpty())
			t.setSEND_EMAILID("- NIL -");

		if (null == t.getSEND_FNAME() || t.getSEND_FNAME().isEmpty())
			t.setSEND_FNAME("- NIL -");

		if (null == t.getSEND_IDREF() || t.getSEND_IDREF().isEmpty())
			t.setSEND_IDREF("- NIL -");

		if (null == t.getSEND_LNAME() || t.getSEND_LNAME().isEmpty())
			t.setSEND_LNAME("- NIL -");

		if (null == t.getSEND_NAME() || t.getSEND_NAME().isEmpty())
			t.setSEND_NAME("- NIL -");

		if (null == t.getSEND_PHONE() || t.getSEND_PHONE().isEmpty())
			t.setSEND_PHONE("- NIL -");

		if (null == t.getSEND_STATE() || t.getSEND_STATE().isEmpty())
			t.setSEND_STATE("- NIL -");

		if (null == t.getSEND_ZIP() || t.getSEND_ZIP().isEmpty())
			t.setSEND_ZIP("- NIL -");

		if (null == t.getTOT_ASS_VAL() || t.getTOT_ASS_VAL().toString().isEmpty())
			t.setTOT_ASS_VAL(0l);

		if (null == t.getTOT_DECL_VAL() || t.getTOT_DECL_VAL().toString().isEmpty())
			t.setTOT_DECL_VAL(0l);

		if (null == t.getTOT_DUTY() || t.getTOT_DUTY().toString().isEmpty())
			t.setTOT_DUTY(0l);

		if (null == t.getTRANS_MODE() || t.getTRANS_MODE().isEmpty())
			t.setTRANS_MODE("- NIL -");

		if (null == t.getUNIQUE_ID() || t.getUNIQUE_ID().isEmpty())
			t.setUNIQUE_ID("- NIL -");

		return t;
	}

	public List<FPO_ITEM_DET> getListPojo(List<FPO_ITEM_DET> fpoList) {

		for (FPO_ITEM_DET t : fpoList) {
			if (null == t.getGEN_CTH() || t.getGEN_CTH().isEmpty())
				t.setGEN_CTH("- NIL -");

			if (null == t.getNETWT() || t.getNETWT().toString().isEmpty())
				t.setNETWT((float) 0l);

			if (null == t.getNOU() || t.getNOU().toString().isEmpty())
				t.setNOU(0l);

			if (null == t.getDECL_VAL() || t.getDECL_VAL().toString().isEmpty())
				t.setDECL_VAL(0l);

			if (null == t.getCURRCD() || t.getCURRCD().isEmpty())
				t.setCURRCD("- NIL -");

			if (null == t.getCTH() || t.getCTH().isEmpty())
				t.setCTH("- NIL -");

			if (null == t.getCTH_REVISED() || t.getCTH_REVISED().isEmpty())
				t.setCTH_REVISED("- NIL -");

			if (null == t.getORIGCNTRYCD() || t.getORIGCNTRYCD().isEmpty())
				t.setORIGCNTRYCD("- NIL -");
		}
		return fpoList;
	}

	public FPO_ITEM_DET getPojo(FPO_ITEM_DET t) {

		if (null == t.getGEN_CTH() || t.getGEN_CTH().isEmpty())
			t.setGEN_CTH("- NIL -");

		if (null == t.getNETWT() || t.getNETWT().toString().isEmpty())
			t.setNETWT((float) 0l);

		if (null == t.getDECL_VAL() || t.getDECL_VAL().toString().isEmpty())
			t.setDECL_VAL(0l);

		if (null == t.getCURRCD() || t.getCURRCD().isEmpty())
			t.setCURRCD("- NIL -");

		if (null == t.getORIGCNTRYCD() || t.getORIGCNTRYCD().isEmpty())
			t.setORIGCNTRYCD("- NIL -");

		return t;
	}

	public C_CUSITM getPojo(C_CUSITM t) {

		if (null == t.getCUS_SITE() || t.getCUS_SITE().isEmpty())
			t.setCUS_SITE("- NIL -");

		if (null == t.getCUST_ORG_CD() || t.getCUST_ORG_CD().isEmpty())
			t.setCUST_ORG_CD("- NIL -");

		if (null == t.getCUST_ORG_CD() || t.getCUST_ORG_CD().isEmpty())
			t.setCUST_ORG_CD("- NIL -");

		if (null == t.getDECLARATION_PID() || t.getDECLARATION_PID().isEmpty())
			t.setDECLARATION_PID("- NIL -");

		if (null == t.getDESTPOST_ORG_CD() || t.getDESTPOST_ORG_CD().isEmpty())
			t.setDESTPOST_ORG_CD("- NIL -");

		if (null == t.getDOCUMENTS() || t.getDOCUMENTS().isEmpty())
			t.setDOCUMENTS("- NIL -");

		if (null == t.getGROSS_WT() || t.getGROSS_WT().toString().isEmpty())
			t.setGROSS_WT(0l);

		if (null == t.getHANDLING_CLASS_CD() || t.getHANDLING_CLASS_CD().isEmpty())
			t.setHANDLING_CLASS_CD("- NIL -");

		if (null == t.getId() || t.getId().isEmpty())
			t.setId("- NIL -");

		if (null == t.getINS_VAL() || t.getINS_VAL().toString().isEmpty())
			t.setINS_VAL(0l);

		if (null == t.getINS_VAL_CURRCD() || t.getINS_VAL_CURRCD().isEmpty())
			t.setINS_VAL_CURRCD("- NIL -");

		if (null == t.getJOB_NO() || t.getJOB_NO().toString().isEmpty())
			t.setJOB_NO(0l);

		if (null == t.getLOCALID() || t.getLOCALID().isEmpty())
			t.setLOCALID("- NIL -");

		if (null == t.getLOCALID2() || t.getLOCALID2().isEmpty())
			t.setLOCALID2("- NIL -");

		if (null == t.getMAIL_CATEGORY_CD() || t.getMAIL_CATEGORY_CD().isEmpty())
			t.setMAIL_CATEGORY_CD("- NIL -");

		if (null == t.getMAIL_STATE_REMARKS() || t.getMAIL_STATE_REMARKS().isEmpty())
			t.setMAIL_STATE_REMARKS("- NIL -");

		if (null == t.getMAIL_STATE_CD() || t.getMAIL_STATE_CD().isEmpty())
			t.setMAIL_STATE_CD("- NIL -");

		if (null == t.getMESG_TYPE_CD() || t.getMESG_TYPE_CD().isEmpty())
			t.setMESG_TYPE_CD("- NIL -");

		if (null == t.getNATURE_TYPE_CD() || t.getNATURE_TYPE_CD().isEmpty())
			t.setNATURE_TYPE_CD("- NIL -");

		if (null == t.getNATURE_TYPE_CD() || t.getNATURE_TYPE_CD().isEmpty())
			t.setNATURE_TYPE_CD("- NIL -");

		if (null == t.getORIGPOST_ORGCD() || t.getORIGPOST_ORGCD().isEmpty())
			t.setORIGPOST_ORGCD("- NIL -");

		if (null == t.getPOST_ORG_CD() || t.getPOST_ORG_CD().isEmpty())
			t.setPOST_ORG_CD("- NIL -");

		if (null == t.getPOSTAGE_AMT() || t.getPOSTAGE_AMT().toString().isEmpty())
			t.setPOSTAGE_AMT(0l);

		if (null == t.getPOSTAGE_CURR_CD() || t.getPOSTAGE_CURR_CD().isEmpty())
			t.setPOSTAGE_CURR_CD("- NIL -");

		if (null == t.getPOSTINGDT() || t.getPOSTINGDT().isEmpty())
			t.setORIGPOST_ORGCD("- NIL -");

		if (null == t.getRECP_ADD1() || t.getRECP_ADD1().isEmpty())
			t.setRECP_ADD1("- NIL -");

		if (null == t.getRECP_ADD2() || t.getRECP_ADD2().isEmpty())
			t.setRECP_ADD2("- NIL -");

		if (null == t.getRECP_CITY() || t.getRECP_CITY().isEmpty())
			t.setRECP_CITY("- NIL -");

		if (null == t.getRECP_CNTRY_CD() || t.getRECP_CNTRY_CD().isEmpty())
			t.setRECP_CNTRY_CD("- NIL -");

		if (null == t.getRECP_EMAILID() || t.getRECP_EMAILID().isEmpty())
			t.setRECP_EMAILID("- NIL -");

		if (null == t.getRECP_FAX() || t.getRECP_FAX().isEmpty())
			t.setRECP_FAX("- NIL -");

		if (null == t.getRECP_FNAME() || t.getRECP_FNAME().isEmpty())
			t.setRECP_FNAME("- NIL -");

		if (null == t.getRECP_FNAME() || t.getRECP_FNAME().isEmpty())
			t.setRECP_FNAME("- NIL -");

		if (null == t.getRECP_IDREF() || t.getRECP_IDREF().isEmpty())
			t.setRECP_IDREF("- NIL -");

		if (null == t.getRECP_LNAME() || t.getRECP_LNAME().isEmpty())
			t.setRECP_LNAME("- NIL -");

		if (null == t.getRECP_PHONE() || t.getRECP_PHONE().isEmpty())
			t.setRECP_PHONE("- NIL -");

		if (null == t.getRECP_STATE() || t.getRECP_STATE().isEmpty())
			t.setRECP_STATE("- NIL -");

		if (null == t.getRECP_ZIP() || t.getRECP_ZIP().isEmpty())
			t.setRECP_ZIP("- NIL -");

		if (null == t.getSEND_ADD1() || t.getSEND_ADD1().isEmpty())
			t.setSEND_ADD1("- NIL -");

		if (null == t.getSEND_ADD2() || t.getSEND_ADD2().isEmpty())
			t.setSEND_ADD2("- NIL -");

		if (null == t.getSEND_CITY() || t.getSEND_CITY().isEmpty())
			t.setSEND_CITY("- NIL -");

		if (null == t.getSEND_CNTRY_CD() || t.getSEND_CNTRY_CD().isEmpty())
			t.setSEND_CNTRY_CD("- NIL -");

		if (null == t.getSEND_CNTRY_CD() || t.getSEND_CNTRY_CD().isEmpty())
			t.setSEND_CNTRY_CD("- NIL -");

		if (null == t.getSEND_EMAILID() || t.getSEND_EMAILID().isEmpty())
			t.setSEND_EMAILID("- NIL -");

		if (null == t.getSEND_FNAME() || t.getSEND_FNAME().isEmpty())
			t.setSEND_FNAME("- NIL -");

		if (null == t.getSEND_IDREF() || t.getSEND_IDREF().isEmpty())
			t.setSEND_IDREF("- NIL -");

		if (null == t.getSEND_LNAME() || t.getSEND_LNAME().isEmpty())
			t.setSEND_LNAME("- NIL -");

		if (null == t.getSEND_NAME() || t.getSEND_NAME().isEmpty())
			t.setSEND_NAME("- NIL -");

		if (null == t.getSEND_PHONE() || t.getSEND_PHONE().isEmpty())
			t.setSEND_PHONE("- NIL -");

		if (null == t.getSEND_STATE() || t.getSEND_STATE().isEmpty())
			t.setSEND_STATE("- NIL -");

		if (null == t.getSEND_ZIP() || t.getSEND_ZIP().isEmpty())
			t.setSEND_ZIP("- NIL -");

		if (null == t.getTRANS_MODE() || t.getTRANS_MODE().isEmpty())
			t.setTRANS_MODE("- NIL -");

		if (null == t.getTYPE_CD() || t.getTYPE_CD().isEmpty())
			t.setTYPE_CD("- NIL -");

		if (null == t.getUNIQUE_ID() || t.getUNIQUE_ID().isEmpty())
			t.setUNIQUE_ID("- NIL -");

		return t;
	}

	/*
	 * public FPO_ITEM_DET getPojo(FPO_ITEM_DET t) {
	 * 
	 * if (null == t.getBCD_AMT() || t.getBCD_AMT().toString().isBlank() ||
	 * t.getBCD_AMT().toString().isEmpty()) t.setBCD_AMT(0l);
	 * 
	 * return t; }
	 * 
	 * public Asstreport getPojo(Asstreport t) { return t; }
	 */
}
